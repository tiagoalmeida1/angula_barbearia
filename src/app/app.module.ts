import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { CadastroComponent } from './cadastro/cadastro.component';

import {APP_ROTAS} from './rotas';
import { MenuComponent } from './menu/menu.component';
import { HttpModule } from '@angular/http';
import { ClienteComponent } from './cliente/cliente.component';
import { LoginComponent } from './login/login.component';
import {BancoService} from './banco.service';

import {FormsModule} from '@angular/forms';
import { ListaComponent } from './lista/lista.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CadastroComponent,
    ClienteComponent,
    LoginComponent,
    ListaComponent
  
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROTAS),
    HttpModule,
    FormsModule
  ],
  providers: [
    BancoService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
