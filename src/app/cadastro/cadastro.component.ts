import { Component, OnInit } from '@angular/core';
import { Cliente } from '../Cliente';
import { BancoService } from '../banco.service';


@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  cliente : Cliente = new Cliente(1, "Vando","1111","111","111","111","111");
  
    constructor(private enviar : BancoService) { }
  
    ngOnInit() {
    }
  
     enviarDados(){
      alert('Nome: ' + this.cliente.nome);
  
      this.enviar.cadastrarClienteAPI(this.cliente)
                       .subscribe(response => this.cliente = response);
    }
}
