import { Component, OnInit } from '@angular/core';
import { BancoService } from '../banco.service';
import { Cliente } from '../Cliente';
import { Fila } from '../fila';
import { Item } from '../item';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
    //clientes: Cliente[] = new Array();
    cliente: Cliente;
    fila: Fila;
    status : String;
  constructor(private banco: BancoService) { 

   this.cliente = new Cliente(1,"Moises","26/10/2017 - 09:30","(85)30354489","Moises","123456","Forada da Fila");
    this.status = "Aguardando";
    this.fila = banco.getFila();
    console.log(this.fila);

  //  this.banco.getLista().subscribe(res => this.clientes = res);
  }

   entrarFila(){
    var  item: Item = new Item(this.cliente, this.fila.itens.length+1, "26/10/2017 - 11:31");
    this.fila.adcionarItem(item);
    this.cliente.status = this.status;
   }

   sairFila(){
      this.cliente.status = "Fora da Fila";
   }
  ngOnInit() {
  }

}
