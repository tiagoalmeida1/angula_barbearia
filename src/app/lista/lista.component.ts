import { Component, OnInit } from '@angular/core';
import {BancoService} from '../banco.service';
import { Cliente } from '../Cliente';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  
  cliente: Cliente[] = new Array();  
  
  constructor(private lista : BancoService) {
    this.lista.getLista().subscribe(res => this.cliente = res)
  }

  

  ngOnInit() {
  }

}
