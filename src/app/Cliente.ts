export class Cliente {
    cod: Number;
    nome: String;
    data: String;
    telefone: String;
    login: String;
    senha: String;
    status: String;

    constructor(
            ccod: Number, cnome: String, cdata: String, ctelefone: String, clogin: String, csenha: String, cstatus: String
        ){
            this.cod = ccod;
            this.nome = cnome;
            this.data = cdata;
            this.telefone = ctelefone;
            this.login = clogin;
            this.senha = csenha;
            this.status = cstatus;
    }
}
