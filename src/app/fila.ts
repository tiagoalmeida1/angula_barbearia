import {Cliente} from './Cliente';
import {Item} from './item';

export class Fila {
    itens: Item[] = new Array();

    tira: Fila;

    adcionarItem(item:Item){
        this.itens.push(item);
    }

}