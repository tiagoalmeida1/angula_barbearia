import { Injectable } from '@angular/core';
import { Cliente } from './Cliente';
import {Item} from  './item';
import {Fila} from  './fila';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class BancoService {
 

 urlAPI : string = "http://localhost:3000/Cliente/"; 
  constructor(private http : Http) { }

  getBanco() : Cliente[] {
     var clientes : Cliente[] = new Array();

     clientes.push(new Cliente(1,"Fulano","25/10/2017","99999999","vander","1234546","Aguardando")); 
     clientes.push(new Cliente(2,"Fulano","25/10/2017","99999999","vander","1234546","Aguardando"));
     clientes.push(new Cliente(3,"Fulano","25/10/2017","99999999","vander","1234546","Aguardando")); 
     return clientes;

  }

  getLista(){
    return this.http.get(this.urlAPI)
    .map(response => response.json());
}


  cadastrarClienteAPI(c : Cliente) {
    
        let body = JSON.stringify(c);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
    
        return this.http.post(this.urlAPI, body, options)
          .map(res => res.json())
      }
  
  getFila(){
    var c0: Cliente = new Cliente(1,"Fulano","25/10/2017","99999999","vander","1234546","Aguardando");
    var ordem : Number = 1;
    var item :Item = new Item(c0, ordem, "26/10/17 - 09:46");
    
    var fila: Fila = new Fila();
    
    fila.adcionarItem(item);
    
    return fila;
  }
}