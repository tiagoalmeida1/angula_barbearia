import {Cliente} from './Cliente';

export class Item {
    cliente: Cliente;
    ordem : Number;
    dataHora: String;

    constructor (cliente:Cliente, ordem : Number, dataHora:String){
        this.cliente = cliente;
        this.ordem = ordem;
        this.dataHora = dataHora;
    }
}