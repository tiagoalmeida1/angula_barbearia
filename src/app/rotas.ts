import {RouterModule, Routes} from '@angular/router';

import { CadastroComponent } from './cadastro/cadastro.component';
import { LoginComponent } from './login/login.component';
//import { AdmComponent } from './adm/adm.component';
import { ListaComponent } from './lista/lista.component';
import { ClienteComponent } from './cliente/cliente.component';
export const APP_ROTAS : Routes = [
    {path : 'login', component : LoginComponent},
   // {path : 'adm', component : AdmComponent},
    {path : 'lista', component : ListaComponent},
    {path : 'cliente', component : ClienteComponent},
    {path : 'home', component : LoginComponent},
    {path : '', component : LoginComponent},
    {path : 'cadastro', component : CadastroComponent}
]; 